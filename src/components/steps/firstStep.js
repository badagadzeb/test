import React, { Component } from "react";
import classname from "classname";

export class FirstStep extends Component {
  render() {
    return (
      <form>
        <div className="row">
          <div className="col-sm-6">
            <div className="form-group defaultInput">
              <input
                type="text"
                onChange={e => this.props.onChange(e)}
                placeholder="სახელი"
                name="name"
                value={this.props.name}
                className={classname("form-control", {
                  "is-invalid": this.props.errors.name
                })}
              />
              <div className="invalid-feedback">{this.props.errors.name}</div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group defaultInput">
              <input
                type="text"
                placeholder="გვარი"
                name="lastName"
                onChange={e => this.props.onChange(e)}
                value={this.props.lastName}
                className={classname("form-control", {
                  "is-invalid": this.props.errors.lastName
                })}
              />
              {/* <img alt="alt" src="./imgs/OL_Log_In_Name.svg" /> */}
              <div className="invalid-feedback">
                {this.props.errors.lastName}
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group defaultInput">
              <select
                onChange={e => this.props.onChange(e)}
                name="selectedbirthdate"
                className={classname("form-control", {
                  "is-invalid": this.props.errors.selectedbirthdate
                })}
                value={this.props.selectedbirthdate}
              >
                <option className="d-none">დაბადების თარიღი</option>
                {this.props.birthdate.map((el, i) => {
                  return (
                    <option key={i} value={el}>
                      {el}
                    </option>
                  );
                })}
              </select>
              {/* <img alt="alt" src="./imgs/OL_Log_In_Name.svg" /> */}
              <div className="invalid-feedback">
                {this.props.errors.selectedbirthdate}
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group defaultInput costumDropDown ">
              <input
                type="text"
                onClick={e => this.props.openSelectF(e)}
                onChange={e => {
                  this.props.onChange(e);
                }}
                placeholder="აირჩიე ქალაქი"
                name="selectedCt"
                value={this.props.selectedCt}
                className={classname("form-control", {
                  "is-invalid": this.props.errors.selectedCtId
                })}
              />
              {this.props.openSelectP && (
                <ul className="">
                  {this.props.cts.map((el, i) => {
                    return (
                      <>
                        {el.label.includes(this.props.selectedCt) ? (
                          <li key={i} onClick={e => this.props.selectCt(e, el)}>
                            {el.label}
                          </li>
                        ) : null}
                      </>
                    );
                  })}
                </ul>
              )}
              <div className="invalid-feedback">
                {this.props.errors.selectedCtId}
              </div>
            </div>
          </div>
        </div>
        <div className="form-group defaultInput">
          <textarea
            type="text"
            onChange={e => this.props.onChange(e)}
            placeholder="დაწერე რამე შენ შესახებ"
            name="aboutYou"
            value={this.props.aboutYou}
            className="form-control"
          />
        </div>
        <label>
          მსურს შეტყობინების მიღება მეილზე{" "}
          <input
            onChange={e => this.props.onChange(e)}
            name="emailNotification"
            checked={this.props.emailNotification}
            type="checkbox"
          />
        </label>
        <div>
          <div />
          <button
            onClick={this.props.nextStep}
            type="button"
            className="btn btn-primary my-4"
          >
            გაგრძელება
          </button>
        </div>
      </form>
    );
  }
}

export default FirstStep;
