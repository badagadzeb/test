import validator from "validator";
import isEmpty from "./is-empty";

export const firstStep = data => {
  const errors = {};
  if (!validator.isLength(data.name, { min: 2 })) {
    errors.name = "სახელი მინიმუმ 2 სიმბოლოსგონ";
  }
  if (!validator.isLength(data.lastName, { min: 2 })) {
    errors.lastName = "გავრი მინიმუმ 3 სიმბოლო";
  }
  if (!data.selectedbirthdate) {
    errors.selectedbirthdate = "მიუთითეთ დაბდღე";
  }
  if (new Date().getFullYear() - data.selectedbirthdate < 18) {
    errors.selectedbirthdate = "უნდა იყო სრულწლოვანი";
  }
  if (!data.selectedCtId) {
    errors.selectedCtId = "აირჩიე ქალაქი";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

export const secodStep = data => {
  const errors = {};
  if (isEmpty(data.itemNaame)) {
    errors.itemNaame = "შეიყვანე ნითის სახელი";
  }
  if (isEmpty(data.itemPrice)) {
    errors.itemPrice = "შეიყვანე ნითის ფასი";
  }
  if (isEmpty(data.itemSum)) {
    errors.itemSum = "შეიყვანე ნითის რაოდენობა";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};

export const editValidation = data => {
  const errors = {};
  if (isEmpty(data.EditItemName)) {
    errors.EditItemName = "შეიყვანე ნითის სახელი";
  }
  if (isEmpty(data.EditItemPrice)) {
    errors.EditItemPrice = "შეიყვანე ნითის ფასი";
  }
  if (isEmpty(data.EditItemSum)) {
    errors.EditItemSum = "შეიყვანე ნითის რაოდენობა";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
