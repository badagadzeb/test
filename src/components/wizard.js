import React, { Component } from "react";
import * as validator from "./validations/validation";
import classname from "classname";
import { connect } from "react-redux";
import shortid from "shortid";
import * as productActions from "../redux/actions/productsActions";
import FirstStep from "./steps/firstStep";

export class Wizard extends Component {
  state = {
    birthdate: [],
    cts: [
      { id: 1, label: "თბილისი" },
      { id: 2, label: "ზუგდიდი" },
      { id: 3, label: "ბათუმი" },
      { id: 4, label: "რუსთავი" },
      { id: 5, label: "ქუთაისი" },
      { id: 6, label: "გორი" },
      { id: 7, label: "ფოთი" },
      { id: 8, label: "ახალციხე" },
      { id: 9, label: "ოზურგეთი" },
      { id: 10, label: "თელავი" },
      { id: 11, label: "ზესტაფონი" },
      { id: 12, label: "თერჯოლა" },
      { id: 13, label: "სამტრედია" },
      { id: 14, label: "საჩხერე" },
      { id: 15, label: "გარდაბანი" },
      { id: 16, label: "ჭიათურა" },
      { id: 17, label: "ახალქალაქი" },
      { id: 18, label: "ქარელი" },
      { id: 19, label: "ხაშური" },
      { id: 20, label: "გურჯაანი" },
      { id: 21, label: "ქობულეთი" },
      { id: 22, label: "მცხეთა" },
      { id: 23, label: "დუშეთი" },
      { id: 24, label: "წალენჯიხა" },
      { id: 25, label: "ჩხოროწყუ" },
      { id: 26, label: "სენაკი" },
      { id: 27, label: "ხობი" },
      { id: 28, label: "აბაშა" },
      { id: 29, label: "მარტვილი" },
      { id: 30, label: "ხარაგაული" },
      { id: 31, label: "ბაღდათი" },
      { id: 32, label: "ვანი" },
      { id: 33, label: "ხონი" },
      { id: 34, label: "წყალტუბო" },
      { id: 35, label: "კასპი" },
      { id: 36, label: "ლაგოდეხი" },
      { id: 37, label: "საგარეჯო" },
      { id: 38, label: "წალკა" },
      { id: 39, label: "მარნეული" },
      { id: 41, label: "ადიგენი" },
      { id: 42, label: "ამბროლაური" },
      { id: 43, label: "ასპინძა" },
      { id: 44, label: "ახმეტა" },
      { id: 45, label: "ბოლნისი" },
      { id: 46, label: "ბორჯომი" },
      { id: 47, label: "დედოფლისწყარო" },
      { id: 48, label: "დმანისი" },
      { id: 49, label: "თეთრი წყარო" },
      { id: 50, label: "თიანეთი" },
      { id: 51, label: "ლანჩხუთი" },
      { id: 52, label: "ლენტეხი" },
      { id: 53, label: "მესტია" },
      { id: 54, label: "ნინოწმინდა" },
      { id: 55, label: "ონი" },
      { id: 56, label: "სიღნაღი" },
      { id: 57, label: "ტყიბული" },
      { id: 58, label: "ქედა" },
      { id: 59, label: "ყაზბეგი" },
      { id: 60, label: "ყვარელი" },
      { id: 61, label: "შუახევი" },
      { id: 62, label: "ჩოხატაური" },
      { id: 63, label: "ცაგერი" },
      { id: 64, label: "ხელვაჩაური" },
      { id: 65, label: "ხულო" }
    ],
    selectedCt: "",
    openSelect: false,
    selectedCtId: null,
    aboutYou: "",
    emailNotification: false,

    name: "",
    lastName: "",
    selectedbirthdate: "",

    errors: {},

    firstStepChecked: false,
    curentStep: 1,

    itemNaame: "",
    itemPrice: "",
    itemSum: "",
    SecondStepChecked: false,

    editableId: null,
    EditItemName: "",
    EditItemPrice: "",
    EditItemSum: ""
  };

  inputref = React.createRef();

  componentDidMount() {
    let birthdate = [];
    for (let i = 2019; i > 1960; i--) {
      birthdate.push(i);
    }
    this.setState({
      birthdate: birthdate
    });

    window.addEventListener("click", () => {
      this.setState({
        openSelect: false
      });
    });
  }
  openSelect = e => {
    e.stopPropagation();
    this.setState({
      openSelect: true
    });
  };

  selectCt = (e, ctObj) => {
    e.stopPropagation();
    this.setState(
      {
        selectedCt: ctObj.label,
        selectedCtId: ctObj.id,
        openSelect: false
      },
      () => {
        if (this.state.firstStepChecked) {
          this.validateFunc(
            {
              name: this.state.name,
              lastName: this.state.lastName,
              selectedbirthdate: this.state.selectedbirthdate,
              selectedCtId: this.state.selectedCtId
            },
            "firstStep"
          );
        }
      }
    );
  };

  validateFunc = (data, funcName) => {
    const { errors, isValid } = validator[funcName](data);
    if (isValid) {
      this.setState({
        errors: {}
      });
      return true;
    } else {
      this.setState({
        errors: errors
      });
      return false;
    }
  };

  nextStep = () => {
    let validated = this.validateFunc(
      {
        name: this.state.name,
        lastName: this.state.lastName,
        selectedbirthdate: this.state.selectedbirthdate,
        selectedCtId: this.state.selectedCtId
      },
      "firstStep"
    );
    if (validated) {
      this.setState({
        curentStep: 2
      });
    } else {
      this.setState({
        firstStepChecked: true
      });
    }
  };

  onChange = e => {
    if (e.target.name === "selectedCt") {
      this.setState({
        selectedCtId: null
      });
    }
    if (e.target.type === "checkbox") {
      if (this.state[e.target.name] === false) {
        this.setState({
          [e.target.name]: true
        });
      } else {
        this.setState({
          [e.target.name]: false
        });
      }
      return;
    }
    this.setState(
      {
        [e.target.name]: e.target.value
      },
      () => {
        if (this.state.firstStepChecked && this.state.curentStep === 1) {
          this.validateFunc(
            {
              name: this.state.name,
              lastName: this.state.lastName,
              selectedbirthdate: this.state.selectedbirthdate,
              selectedCtId: this.state.selectedCtId
            },
            "firstStep"
          );
        }
        if (this.state.SecondStepChecked && this.state.curentStep === 2) {
          this.validateFunc(
            {
              itemNaame: this.state.itemNaame,
              itemPrice: this.state.itemPrice,
              itemSum: this.state.itemSum
            },
            "secodStep"
          );
        }
      }
    );
  };

  editItem = el => {
    this.setState({
      editableId: el.id,
      EditItemName: el.itemNaame,
      EditItemPrice: el.itemPrice,
      EditItemSum: el.itemSum
    });
  };

  editProduct = () => {
    let validated = this.validateFunc(
      {
        EditItemName: this.state.EditItemName,
        EditItemPrice: this.state.EditItemPrice,
        EditItemSum: this.state.EditItemSum
      },
      "editValidation"
    );
    if (validated) {
      this.props.editProduct({
        itemNaame: this.state.EditItemName,
        itemPrice: this.state.EditItemPrice,
        itemSum: this.state.EditItemSum,
        id: this.state.editableId
      });
      this.setState({
        editableId: null
      });
    }
  };
  secondStep = () => {
    return (
      <>
        <form>
          <div className="row">
            <div className="col-sm-4">
              <div className="form-group defaultInput">
                <input
                  type="text"
                  ref={this.inputref}
                  onChange={this.onChange}
                  placeholder="ნივთის სახელი"
                  name="itemNaame"
                  value={this.state.itemNaame}
                  className={classname("form-control", {
                    "is-invalid": this.state.errors.itemNaame
                  })}
                />
                <div className="invalid-feedback">
                  {this.state.errors.itemNaame}
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="form-group defaultInput">
                <input
                  type="number"
                  onChange={this.onChange}
                  placeholder="ნივთის ფასი"
                  name="itemPrice"
                  value={this.state.itemPrice}
                  className={classname("form-control", {
                    "is-invalid": this.state.errors.itemPrice
                  })}
                />
                <div className="invalid-feedback">
                  {this.state.errors.itemPrice}
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="form-group defaultInput">
                <input
                  type="number"
                  onChange={this.onChange}
                  placeholder="რაოდენობა"
                  name="itemSum"
                  value={this.state.itemSum}
                  className={classname("form-control", {
                    "is-invalid": this.state.errors.itemSum
                  })}
                />
                <div className="invalid-feedback">
                  {this.state.errors.itemSum}
                </div>
              </div>
            </div>
            <div className="col-12 text-center">
              <button onClick={this.addItem} className="btn btn-primary mb-5">
                დამატება
              </button>
            </div>
          </div>
        </form>
        <div className="product table">
          <table className="table">
            <thead>
              <tr>
                <th>ნივთის სახელი</th>
                <th>ნივთის ფასი</th>
                <th>რაოდენობა</th>
              </tr>
            </thead>
            <tbody>
              {this.props.products.products.map((el, i) => {
                return (
                  <>
                    <tr key={el.id}>
                      <td>
                        {this.state.editableId !== el.id ? (
                          el.itemNaame
                        ) : (
                          <div className="form-group defaultInput">
                            <input
                              type="text"
                              onChange={this.onChange}
                              placeholder="ნივთის სახელი"
                              name="EditItemName"
                              value={this.state.EditItemName}
                              className={classname("form-control", {
                                "is-invalid": this.state.errors.EditItemName
                              })}
                            />
                            <div className="invalid-feedback">
                              {this.state.errors.EditItemName}
                            </div>
                          </div>
                        )}
                      </td>
                      <td>
                        {" "}
                        {this.state.editableId !== el.id ? (
                          el.itemPrice + " " + "₾"
                        ) : (
                          <div className="form-group defaultInput">
                            <input
                              type="number"
                              onChange={this.onChange}
                              placeholder="ნივთის ფასი"
                              name="EditItemPrice"
                              value={this.state.EditItemPrice}
                              className={classname("form-control", {
                                "is-invalid": this.state.errors.EditItemPrice
                              })}
                            />
                            <div className="invalid-feedback">
                              {this.state.errors.EditItemPrice}
                            </div>
                          </div>
                        )}
                      </td>
                      <td>
                        {this.state.editableId !== el.id ? (
                          el.itemSum
                        ) : (
                          <div className="form-group defaultInput">
                            <input
                              type="number"
                              onChange={this.onChange}
                              placeholder="რაოდენობა"
                              name="EditItemSum"
                              value={this.state.EditItemSum}
                              className={classname("form-control", {
                                "is-invalid": this.state.errors.EditItemSum
                              })}
                            />
                            <div className="invalid-feedback">
                              {this.state.errors.EditItemSum}
                            </div>
                          </div>
                        )}
                      </td>
                      <td className="controlS">
                        <span
                          onClick={() => this.props.deleteItem(el.id)}
                          className="deleteItem"
                        >
                          <img
                            alt=""
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEX///8AAACpqanj4+NZWVmsrKz8/PwsLCz09PTd3d05OTmampoVFRVjY2Pv7+/5+fnV1dW6urqzs7Nra2uNjY0aGhrp6el3d3dERESAgIDIyMgiIiI/Pz/W1tZlZWWLi4tQUFAmJiYNDQ2enp65ubmUlJTMzMx5eXlBQUFNTU3MHuHxAAAGtElEQVR4nO2d6WKiMBCAxbteFK+qVWtF3d33f8EtpbUzISFmCEnYne9vI8xXQhJytlquGCbtxWh1js6rwTG9rJ3d1w392/YcCbzsYt9hWWM9Hoh6n6zeOr5Ds8JwK9XLWfwDuXX+WiL4Qeo7wIrMpuV+H2wanVVvWr+MxHeYdHoPCUbR1XegVNIHBT9qDt+h0rg+LNhQxUezaM7Sd7jm7IwEo2juO2BTZrJqsDtuJ7ekly5kik2rNIr14CkB7dBdUXLjL1gKFzH+o/iIZr/EJI1q3cQrHPzrTpLoMBIUZ87jpCNUFCd57H0hqzaoyujjyLvKhEJObc5DbKO49yUpuyhlcypFVFOch2VJ8bexqwCrckBR30rTPqG0sgIpRN5g0AtN4iVMfHQSX3VQztM1VWYw8cRJfJXpmD0V1I3z7CC+6qA2t/7NeobJm9H+hnXFa2lB+kl/D9L7r/SHMy0xbKl09T+IX0D6Tay/gf6fRmM9X+wnhY5rP5xHx/aTZb1U0+vpg6U9yY70izUATnaK3tJeed/YGBVIVvr7+KRd0Q8VdGEyrfTNFZ98x/8Agwo5FVXG4TKhP0WxIyVUyj9BS2hCFs0Z9EmCJuMOviG1Z5/11w0Iwhhkf1O4ynF8uXX8c0jSYh32bv4qtsVrXEPq64vbEyE84366vlCOdkPyy4iFxqRxeSoMPIxribIacxyi6bAH7qrt1RJiVfAQ5cjsx2v8CtYTYWXwMLPZp1Ra4b/jEJTTzMoaNMBZ3mvtE9RxWTY+UoT+S7ccYaAmTTfUER1mMZODChuTFxH9MLSaEAEDNelOhoVU2GMJ8AvWpEaETbZTTbHZAbZQTSo1aKgeow4BODhn8gnVTEPdUCWkObmU+gxhSRNuiyYDfmG8GfwugYVwbdHZAH6nm5SlaK7BobbwqhPDQE2aJuiHoX5ZZKDMZtR+RoNpdYVnAdThadSliLp6wm2Yov5Asy8E1ENwDnaBEupMMvs+HMKfBjuRB01NigyHhHGP5LaeCCsyRjGazjEWerwXAWZUoTvReCoO7myLBqF1ZTwJffID42dQGLY43uqayGJOfChMnriYX6Uw0zwaHLfXtn/G20VxYJPyBTQLZHrQY5BWa5iuevEJsVEy1l85EH7RBGWvYphMqYJivR8qFQSFWdiBsqVNU/gm0d/BM1WnfbU6+tXYPtnbmIK5C3juXuUH+MUlzMlRK1t+LWEsKhhsTuxHg95dn8CmpM3eFTTWavG65gxcGFarfirChlTY0B1sSIUN3cGGVNjQHWxIhQ3dwYZU2NAdbEiFDd3BhlTY0B1sSIUN3cGGVNjQHWxIhQ3dwYZU2NAdbEjlUcNZch2XHq7S6S3T0uW6t3Q5L5sr6tlw/DlT4l25k8own7t8Uioc8kWvJTPV/Bq+aG49fP9OoFgsdj8O448yEq+GYJ2NfPetn9nnA+nfwRbSyvUvXg3BsmrpChQ47Ui6rSlYuarcvsunIUwjfUZwYap0uSdc4qEqjnwawl2dV7I0cNq4dFEyXFuvmrMWiuFEtvgEGkonLsMZkKpph/+OoSp6NqTChhlsyIZsmMOGVNgwgw3ZkA1z2JAKG2awIRuyYQ4bUmHDDDZkQzbMYUMqbJjBhmzIhjlsSIUNM9iQDdkwhw2psGEGG7IhG+awIRU2zGBDNmTDHDakwoYZbMiGbJjDhlTYMIMN2ZANc9iQChtmsCEb/m+Goa+Sla50TkEC6Urn3yBBiCud4XJ76Wp1ePKndEeBI0gQ4mp1eK6rVACe/CkVACcvvatOIvRqePtJIo/vev+74iy/nxdReUaO350/7kWJ4rzL/vfRUe+KPSHuGV19hrjn3Vtun7u3qDdn+SpstsodbL62dyk5BMj3Djytp2Sn2rMj55bsSvcoindJ6Q493g1rpy7D9f9lWJ4Na2YCAlHu1kQAGSq2QHIDDES6jQ8RdK6szX+dKeiUYtL5sQpi2GSUtikd0YaGVl8XeITy2ePZzvDzY2/1rHD4XRClNq9sBDqiWN3woXCAl155K03hy2L1+MOPF3FU33/vcfARzPJN4cjgw+j95FN8ri3lrPEyntDVo6vlyz9CD4dgvdLq4utPnbfdhPOl5RsTVkF4iHZbhXou4u0v9u+xEO8RbZP10AXr3dtEvPfGvmArLhj6xGaL7c5Of19n1FSYX/V3dkRtTePiq+iHTX0FeRjnrI+sNrkFQniKf+qtipe+/epvFCf6GGrFQUNj7TOnTmupBws8+3Kc2ux7Kmc9fnWud166eX4/kvPtSB+WJQYvPbLeX51HhM6XUuyyAAAAAElFTkSuQmCC"
                          />
                        </span>
                        <span
                          onClick={() => this.editItem(el)}
                          className="editItem"
                        >
                          <img
                            alt=""
                            src="https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/new-24-512.png"
                          />
                        </span>
                      </td>
                    </tr>
                    {this.state.editableId !== el.id ? null : (
                      <>
                        {" "}
                        <div className="text-center w-100 ml-2">
                          {" "}
                          <button
                            onClick={this.editProduct}
                            className="btn btn-success btn-sm mr-3"
                          >
                            შენახვა{" "}
                          </button>{" "}
                          <button
                            onClick={() => {
                              this.setState({
                                editableId: null
                              });
                            }}
                            className="btn btn-danger btn-sm"
                          >
                            გაუქმება
                          </button>{" "}
                        </div>
                      </>
                    )}
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  };

  addItem = e => {
    e.preventDefault();
    let validated = this.validateFunc(
      {
        itemNaame: this.state.itemNaame,
        itemPrice: this.state.itemPrice,
        itemSum: this.state.itemSum
      },
      "secodStep"
    );
    if (validated) {
      this.props.saveitem({
        itemNaame: this.state.itemNaame,
        itemPrice: this.state.itemPrice,
        itemSum: this.state.itemSum,
        id: shortid.generate()
      });
      this.setState({
        itemNaame: "",
        itemPrice: "",
        itemSum: "",
        SecondStepChecked: false
      });
      this.inputref.current.focus();
    } else {
      this.setState({
        SecondStepChecked: true
      });
    }
  };

  render() {
    return (
      <div className="wrapper">
        <div className="container wizard ">
          <h2 className="text-center">რაღაც ტექსტი </h2>
          <ul className="steps d-flex  list-unstyled wizardSteps pt-4 mb-3">
            <li
              onClick={() =>
                this.setState({
                  curentStep: 1
                })
              }
              className={classname(
                "d-flex align-items-center justify-content-center",
                {
                  active: this.state.curentStep === 1,
                  done: this.state.curentStep > 1
                }
              )}
            >
              1<span className="stepName">პირველი ეტაპი</span>
            </li>
            <li
              className={classname(
                "d-flex align-items-center justify-content-center",
                {
                  active: this.state.curentStep === 2
                }
              )}
            >
              2<span className="stepName">მეორე ეტაპი</span>
            </li>
          </ul>
          {this.state.curentStep === 1 ? (
            <FirstStep
              lastName={this.state.lastName}
              name={this.state.name}
              errors={this.state.errors}
              selectedbirthdate={this.state.selectedbirthdate}
              birthdate={this.state.birthdate}
              selectedCt={this.state.selectedCt}
              openSelectP={this.state.openSelect}
              openSelectF={e => this.openSelect(e)}
              aboutYou={this.state.aboutYou}
              onChange={e => this.onChange(e)}
              cts={this.state.cts}
              nextStep={this.nextStep}
              selectCt={(e, el) => this.selectCt(e, el)}
              emailNotification={this.state.emailNotification}
            />
          ) : this.state.curentStep === 2 ? (
            this.secondStep()
          ) : (
            ":/"
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  saveitem: data => dispatch(productActions.SveItem(data)),
  deleteItem: id => dispatch(productActions.DeleteItem(id)),
  editProduct: data => dispatch(productActions.EditProduct(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Wizard);
