export function SveItem(item) {
  return { type: "SAVE_ITEM", payload: item };
}

export function DeleteItem(id) {
  return { type: "DELETE_ITEM", payload: id };
}

export function EditProduct(data) {
  return { type: "EDIT_PRODUCT", payload: data };
}
